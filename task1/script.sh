#!/bin/bash

rep=$1
b1=$2
b2=$3

git clone $rep &> /dev/null

name=`echo "$rep" | sed -r "s/.*\/(.*)\.git/\1/"`
cd $name

{
    git checkout $b1
    git checkout $b2
    git checkout master
} &> /dev/null

cd ../
dir=`pwd`
touch "$dir"/difference.txt
cd $name
git diff --name-only $b1...$b2 > "$dir"/difference.txt
