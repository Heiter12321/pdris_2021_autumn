#!/usr/bin/env bash

dir=`dirname $0`
mem_pid=`cat $dir/pid.txt 2> /dev/null`
command=$1
command=${command^^}

if [[ $command == "START" ]]; then
if [ -z $mem_pid ]; then
    rm -rf "$dir/free_memory.csv"
    ("$dir/script.sh" "$dir/free_memory.csv")&
    mem_pid=$!
    echo $mem_pid > "$dir/pid.txt"
    echo "$mem_pid started"
else
    echo "already started"
fi

elif [[ $command == "STATUS" ]]; then
if [ ! -z $mem_pid ]; then
    echo "$mem_pid running"
else
    echo "chill"
fi

elif [[ $command == "STOP" ]]; then
if [ ! -z $mem_pid ]; then
    kill -9 $mem_pid 2> /dev/null
    echo "$mem_pid stopped"
    rm -rf "$dir/pid.txt"
else
    echo "nobody running"
fi

else
    echo "What?"
fi

