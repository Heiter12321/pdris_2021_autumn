#!/usr/bin/env bash
while true
do
    cur_time=$(date '+%H:%M:%S')
    timestamp=`echo -n "$cur_time;"`
    free=`free -m | awk 'NR==2{printf "%sMB;%sMB;%.2f%% \n", $2,$2-$3,$3*100/$2 }'`
    echo "${timestamp}${free}" >> free_memory.csv
    sleep 10m
done
